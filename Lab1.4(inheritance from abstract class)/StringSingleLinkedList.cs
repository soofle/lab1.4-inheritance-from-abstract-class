﻿namespace Lab1._4_inheritance_from_abstract_class_
{
    public class StringSingleLinkedList : SingleLinkedListBase
    {
        public void Add(string value)
        {
            AddToObjectList(value);
        }

        public bool Remove(string value)
        {
            return RemoveFromObjectList(value);
        }

        public override bool IsEqual(object value1, object value2)
        {
            return (string)value1 == (string)value2;
        }
    }
}