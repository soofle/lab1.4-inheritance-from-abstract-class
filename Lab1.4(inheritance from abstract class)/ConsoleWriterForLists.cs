﻿using System;

namespace Lab1._4_inheritance_from_abstract_class_
{
    public class ConsoleWriterForLists
    {
        public void OutputList(SingleLinkedListBase list)
        {
            if (list.First == null)
            {
                Console.WriteLine("The list is empty!");
                return;
            }

            Console.WriteLine("Our list:");

            var current = list.First;
            while (current != null)
            {
                Console.Write("{0} ",current.Value);
                current = current.Next;
            }

            Console.WriteLine();
        }
    }
}