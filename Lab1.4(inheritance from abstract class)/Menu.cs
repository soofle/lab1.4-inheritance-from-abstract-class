﻿using System;
using System.Text;

namespace Lab1._4_inheritance_from_abstract_class_
{
    public static class Menu
    {
        public static void Start()
        {
            Console.OutputEncoding=Encoding.UTF8;
            bool end = false;
            while (end != true)
            {
                Console.WriteLine("Выберите List:\n" +
                                  "1. Односвязный список для хранения целых чисел.\n" +
                                  "2. Односвязный список для хранения строк.\n" +
                                  "999. Выход.");
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("1");
                        InvokeTaskWithIntSll();
                        Console.Clear();
                        break;
                    case "2":
                        Console.WriteLine("2");
                        InvokeTaskWithStringSll();
                        Console.Clear();
                        break;
                    case "999":
                        end = true;
                        break;
                    default:
                        Console.WriteLine("Введите 1 или 2 или 999");
                        break;
                }
            }
        }

        private static void InvokeTaskWithIntSll()
        {
            Console.Clear();
            Console.WriteLine("Добро пожаловать в List \"Односвязный список для хранения целых чисел\"");

            var intList = new IntSingleLinkedList();
            var consoleWriterForLists =new ConsoleWriterForLists();

            bool end = false;
            while (end != true)
            {
                Console.WriteLine("\nВыберите действие:\n" +
                                  "1. Добавить элемент.\n" +
                                  "2. Удалить элемент.\n" +
                                  "3. Вывести колличество элементов.\n" +
                                  "4. Вывести все элементы на экран\n" +
                                  "9. Вернуться в Главное меню.");
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("Введите \"Значение\" элемента");
                        if (Int32.TryParse(Console.ReadLine(), out int value))
                            intList.Add(value);
                        else
                            Console.WriteLine("Введено не число!");
                        break;
                    case "2":
                        Console.WriteLine("Введите \"Значение\" элемента");
                        if (Int32.TryParse(Console.ReadLine(), out value))
                        {
                            if (intList.Remove(value))
                                Console.WriteLine("{0} удален", value);
                            else
                                Console.WriteLine("{0} не найден", value);
                        }
                        else
                            Console.WriteLine("Введено не число!");
                        break;
                    case "3":
                        Console.WriteLine("Количество элементов: {0}", intList.Count);
                        break;
                    case "4":
                        consoleWriterForLists.OutputList(intList);
                        break;
                    case "9":
                        end = true;
                        break;
                    default:
                        Console.WriteLine("Введите верное значение");
                        break;
                }
            }

        }

        private static void InvokeTaskWithStringSll()
        {
            Console.Clear();
            Console.WriteLine("Добро пожаловать в List \"Односвязный список для хранения строк.\"");

            var stringList = new StringSingleLinkedList();
            var consoleWriterForLists = new ConsoleWriterForLists();

            bool end = false;
            while (end != true)
            {
                Console.WriteLine("\nВыберите действие:\n" +
                                  "1. Добавить элемент.\n" +
                                  "2. Удалить элемент.\n" +
                                  "3. Вывести колличество элементов.\n" +
                                  "4. Вывести все элементы на экран\n" +
                                  "9. Вернуться в Главное меню.");
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("Введите \"Значение\" элемента");
                        string inputData = Console.ReadLine();
                        if (inputData == "")
                        {
                            Console.WriteLine("Введена пустая строка");
                        }
                        else
                            stringList.Add(inputData);
                        break;
                    case "2":
                        Console.WriteLine("Введите \"Значение\" элемента");
                        string value = Console.ReadLine();
                        if (stringList.Remove(value))
                            Console.WriteLine("{0} удален", value);
                        else
                            Console.WriteLine("{0} не найден", value);
                        break;
                    case "3":
                        Console.WriteLine("Количество элементов: {0}", stringList.Count);
                        break;
                    case "4":
                        consoleWriterForLists.OutputList(stringList);
                        break;
                    case "9":
                        end = true;
                        break;
                    default:
                        Console.WriteLine("Введите верное значение");
                        break;
                }
            }
        }
    }
}