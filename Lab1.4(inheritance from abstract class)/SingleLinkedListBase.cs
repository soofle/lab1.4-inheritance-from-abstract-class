﻿namespace Lab1._4_inheritance_from_abstract_class_
{
    public abstract class SingleLinkedListBase
    {
        public ListElement First { get; private set; }
        public int Count { get; private set; }

        public void AddToObjectList(object value)
        {
            if (First == null)
            {
                First = new ListElement(value);
                Count++;
                return;
            }

            ListElement tempListElement = First;
            while (tempListElement.Next != null)
                tempListElement = tempListElement.Next;
            tempListElement.Next = new ListElement(value);
            Count++;
        }

        public bool RemoveFromObjectList(object value)
        {
            if (First == null)
                return false;
            ListElement prev = null;
            ListElement current = First;
            while (current != null)
            {
                if (IsEqual(current.Value,value))
                {
                    if (prev == null)
                    {
                        First = current.Next;
                        Count--;
                        return true;
                    }

                    prev.Next = current.Next;
                    Count--;
                    return true;
                }

                prev = current;
                current = current.Next;
            }

            return false;
        }

        public abstract bool IsEqual(object value1, object value2);
    }
}