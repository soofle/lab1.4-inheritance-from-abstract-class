﻿namespace Lab1._4_inheritance_from_abstract_class_
{
    public class IntSingleLinkedList : SingleLinkedListBase
    {
        public void Add(int value)
        {
            AddToObjectList(value);
        }

        public bool Remove(int value)
        {
            return RemoveFromObjectList(value);
        }

        public override bool IsEqual(object value1, object value2)
        {
            return (int) value1 == (int) value2;
        }
    }
}