﻿namespace Lab1._4_inheritance_from_abstract_class_
{
    public class ListElement
    {
        public ListElement(object value)
        {
            Value = value;
            Next = null;
        }

        public object Value { get; set; }
        public ListElement Next { get; set; }
    }
}